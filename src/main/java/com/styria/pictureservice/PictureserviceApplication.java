package com.styria.pictureservice;

import com.styria.pictureservice.mail.MailUtil;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

@SpringBootApplication
public class PictureserviceApplication {

    public static void main(String[] args) {

        AnnotationConfigApplicationContext ctx = (AnnotationConfigApplicationContext) SpringApplication.run(PictureserviceApplication.class, args);

        MailUtil mailUtil = ctx.getBean("mailUtil",MailUtil.class);

        mailUtil.sendEmail();




    }
}
