package com.styria.pictureservice.configuration;

import com.jcraft.jsch.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Properties;

@Configuration
public class SshClientConfiguration {

    private static String USERNAME ="";

    private static String host = "";

    private static int port=22;

    private static String SSHKEY = "";

    @Bean
    public Session sshSession() {

        JSch jSch = new JSch();

        Properties config = new java.util.Properties();
        config.put("StrictHostKeyChecking", "no");

        try {

            jSch.addIdentity(SSHKEY);

            Session session = jSch.getSession(USERNAME, host);
            session.setConfig(config);

            return session;

        } catch (JSchException e) {
            e.printStackTrace();
            return null;
        }

    }


}
