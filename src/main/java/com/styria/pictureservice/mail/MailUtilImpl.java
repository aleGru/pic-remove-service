package com.styria.pictureservice.mail;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;


import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.List;

@Configuration("mailUtil")
@EnableAsync
@EnableScheduling
public class MailUtilImpl implements MailUtil {

    @Value("#{'${email.to.list}'.split(',')}")
    private List<String> emailToList;

    @Autowired
    public JavaMailSender sender;

   @Override
   @Async
   @Scheduled(fixedRate=5000)
   public void sendEmail() {

        String[] recipients = emailToList.toArray(new String[0]);

        MimeMessage message = sender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);

        try {

            helper.setTo(recipients);
            helper.setSubject("A test mail");
            helper.setText("This is just a test message!");

        } catch (MessagingException e) {

            e.printStackTrace();

        }

        sender.send(message);

    }

}
