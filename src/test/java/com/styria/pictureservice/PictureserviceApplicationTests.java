package com.styria.pictureservice;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PictureserviceApplicationTests {

    @Autowired
    @Qualifier("oracle-octopus")
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private Session session;

    @Test
    public void contextLoads() {
        System.out.println("MyTest");


        assertThat("hallo").isEqualTo("hallo");


    }

    @Test
    public void compare() {
        System.out.println("MyTest");


        assertThat("hallo").isEqualTo("hallo");


    }

    @Test
    public void testConnection() {

        String sql = "select MPLI_ID from SYSTEM_MPLI_RIGHTS where MPLI_ID = 1";

        List<Map<String, Object>> maps = jdbcTemplate.queryForList(sql);

        BigDecimal id = (BigDecimal) maps.get(0).get("MPLI_ID");

        assertThat(BigDecimal.ONE).isEqualTo(id);

    }

    @Test
    public void testSshSession() {

        try {
            session.connect();


            String command="set|grep SSH";
            String command1="ls -al";


            Channel channel = session.openChannel("exec");
            ((ChannelExec) channel).setCommand(command1);

//            channel.setInputStream(null);

            InputStream in=channel.getInputStream();
            OutputStream out=channel.getOutputStream();
            ((ChannelExec)channel).setErrStream(System.err);

            channel.connect();

            byte[] tmp=new byte[1024];
            while(true){
                while(in.available()>0){
                    int i=in.read(tmp, 0, 1024);
                    if(i<0)break;
                    System.out.print(new String(tmp, 0, i));
                }
                if(channel.isClosed()){
                    if(in.available()>0) continue;
                    System.out.println("exit-status: "+channel.getExitStatus());
                    break;
                }
                try{Thread.sleep(1000);}catch(Exception ee){}
            }

            channel.disconnect();
            session.disconnect();



        } catch (JSchException | IOException e) {
            e.printStackTrace();
        }


    }
}